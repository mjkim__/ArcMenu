# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: ArcMenu\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-10-15 08:34-0400\n"
"PO-Revision-Date: 2023-10-21 12:09-0400\n"
"Last-Translator: Wxtewx <wxtewx@qq.com>\n"
"Language-Team: \n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: appMenu.js:63 appMenu.js:318
msgid "Pin to ArcMenu"
msgstr "固定到 ArcMenu"

#: appMenu.js:85 appMenu.js:274
msgid "Create Desktop Shortcut"
msgstr "创建桌面快捷方式"

#: appMenu.js:273
msgid "Delete Desktop Shortcut"
msgstr "删除桌面快捷方式"

#: appMenu.js:282 appMenu.js:318
msgid "Unpin from ArcMenu"
msgstr "从 ArcMenu 取消固定"

#: appMenu.js:357
msgid "Open Folder Location"
msgstr "打开文件夹位置"

#: constants.js:79
msgid "Favorites"
msgstr "收藏夹"

#: constants.js:80 menulayouts/runner.js:130 menulayouts/windows.js:334
#: settings/Menu/LayoutTweaksPage.js:821
msgid "Frequent Apps"
msgstr "常用应用程序"

#: constants.js:81 menuWidgets.js:1128 menulayouts/az.js:63
#: menulayouts/az.js:72 menulayouts/eleven.js:54 menulayouts/eleven.js:63
#: menulayouts/redmond.js:72 menulayouts/redmond.js:75
#: menulayouts/redmond.js:79
msgid "All Apps"
msgstr "所有应用程序"

#: constants.js:82 menulayouts/insider.js:276
#: settings/Menu/LayoutTweaksPage.js:701 settings/Menu/LayoutTweaksPage.js:819
#: settings/MenuPage.js:101
msgid "Pinned Apps"
msgstr "固定应用程序"

#: constants.js:83 menulayouts/raven.js:302 menulayouts/unity.js:373
#: searchProviders/recentFiles.js:29 searchProviders/recentFiles.js:30
msgid "Recent Files"
msgstr "最近的文件"

#: constants.js:242 menuButton.js:732 settings/Menu/ListPinnedPage.js:377
msgid "Log Out..."
msgstr "注销..."

#: constants.js:243 menuButton.js:731 settings/Menu/ListPinnedPage.js:376
msgid "Lock"
msgstr "锁定"

#: constants.js:244 menuButton.js:726 settings/Menu/ListPinnedPage.js:379
msgid "Restart..."
msgstr "重启..."

#: constants.js:245 menuButton.js:727 settings/Menu/ListPinnedPage.js:378
msgid "Power Off..."
msgstr "关机..."

#: constants.js:246 menuButton.js:723 settings/Menu/ListPinnedPage.js:380
msgid "Suspend"
msgstr "暂停"

#: constants.js:247 settings/Menu/ListPinnedPage.js:381
msgid "Hybrid Sleep"
msgstr "混合休眠"

#: constants.js:248 settings/Menu/ListPinnedPage.js:383
msgid "Hibernate"
msgstr "休眠"

#: constants.js:249 menuButton.js:733 settings/Menu/ListPinnedPage.js:384
msgid "Switch User"
msgstr "切换用户"

#: constants.js:391 settings/AboutPage.js:43
msgid "ArcMenu"
msgstr ""

#: constants.js:396
msgid "Brisk"
msgstr ""

#: constants.js:401
msgid "Whisker"
msgstr ""

#: constants.js:406
msgid "GNOME Menu"
msgstr "GNOME 菜单"

#: constants.js:411
msgid "Mint"
msgstr ""

#: constants.js:416
msgid "Budgie"
msgstr ""

#: constants.js:424
msgid "Unity"
msgstr ""

#: constants.js:429
msgid "Plasma"
msgstr ""

#: constants.js:434
msgid "tognee"
msgstr ""

#: constants.js:439
msgid "Insider"
msgstr ""

#: constants.js:444
msgid "Redmond"
msgstr ""

#: constants.js:449
msgid "Windows"
msgstr "窗口"

#: constants.js:454
msgid "11"
msgstr ""

#: constants.js:459
msgid "a.z."
msgstr ""

#: constants.js:464
msgid "Enterprise"
msgstr "企业"

#: constants.js:469
msgid "Pop"
msgstr ""

#: constants.js:477
msgid "Elementary"
msgstr ""

#: constants.js:482
msgid "Chromebook"
msgstr ""

#: constants.js:490
msgid "Runner"
msgstr ""

#: constants.js:495
msgid "GNOME Overview"
msgstr "GNOME 概述"

#: constants.js:503
msgid "Raven"
msgstr ""

#: constants.js:511
msgid "Traditional"
msgstr "传统"

#: constants.js:516
msgid "Modern"
msgstr "现代"

#: constants.js:521
msgid "Touch"
msgstr "触摸"

#: constants.js:526
msgid "Launcher"
msgstr "启动器"

#: constants.js:531
msgid "Alternative"
msgstr "选择"

#: constants.js:538 menulayouts/whisker.js:51
msgid "Settings"
msgstr "设置"

#: constants.js:538
msgid "Software"
msgstr "软件"

#: constants.js:538
msgid "Terminal"
msgstr "终端"

#: constants.js:538
msgid "Tweaks"
msgstr "调整"

#: constants.js:539 menuButton.js:676 menuWidgets.js:497
#: settings/Menu/ListPinnedPage.js:346 settings/Menu/ListPinnedPage.js:366
msgid "Activities Overview"
msgstr "活动概况"

#: constants.js:539 menuButton.js:662 prefs.js:24 settings/AboutPage.js:128
#: settings/Menu/ListPinnedPage.js:334 settings/Menu/ListPinnedPage.js:343
#: settings/Menu/ListPinnedPage.js:353
msgid "ArcMenu Settings"
msgstr "ArcMenu 设置"

#: constants.js:539
msgid "Files"
msgstr "文件"

#: menuButton.js:39 menulayouts/plasma.js:122 menulayouts/plasma.js:559
msgid "Apps"
msgstr "应用"

#: menuButton.js:707 settings/Menu/ListPinnedPage.js:369
msgid "Show Desktop"
msgstr "显示桌面"

#: menuButton.js:721 menuWidgets.js:814 menuWidgets.js:832
msgid "Power Off / Log Out"
msgstr "关机/注销"

#: menuButton.js:762
msgid "Dash to Panel Settings"
msgstr "Dash to Panel 设置"

#: menuButton.js:766
msgid "App Icons Taskbar Settings"
msgstr "应用程序图标任务栏设置"

#: menuWidgets.js:869 menulayouts/plasma.js:321
msgid "Session"
msgstr "会话"

#: menuWidgets.js:873 menulayouts/plasma.js:325
msgid "System"
msgstr "系统"

#: menuWidgets.js:1058 menuWidgets.js:1079 menulayouts/az.js:64
#: menulayouts/eleven.js:55 menulayouts/redmond.js:73 menulayouts/redmond.js:78
#: settings/Menu/SubPage.js:76 settings/Menu/ThemingDialog.js:338
msgid "Back"
msgstr "返回"

#: menuWidgets.js:1798
msgid "New"
msgstr "新建"

#: menuWidgets.js:2486 menulayouts/az.js:71 menulayouts/eleven.js:62
#: menulayouts/insider.js:171 menulayouts/plasma.js:117 menulayouts/raven.js:61
#: menulayouts/raven.js:236 menulayouts/raven.js:284 menulayouts/redmond.js:74
#: menulayouts/redmond.js:77 menulayouts/redmond.js:80 menulayouts/unity.js:39
#: menulayouts/unity.js:278 menulayouts/unity.js:354 menulayouts/windows.js:411
msgid "Pinned"
msgstr "已固定"

#: menuWidgets.js:2651
msgid "Unmount Drive"
msgstr "卸载驱动器"

#: menuWidgets.js:2653
msgid "Eject Drive"
msgstr "弹出驱动器"

#: menuWidgets.js:2697
msgid "Remove from Recent"
msgstr "从最近删除"

#: menuWidgets.js:2779
msgid "Search…"
msgstr "搜索…"

#: menulayouts/baseMenuLayout.js:466
msgid "More Recent Files..."
msgstr "更多最近的文件..."

#: menulayouts/eleven.js:70
msgid "Frequent"
msgstr "常用"

#: menulayouts/plasma.js:126 placeDisplay.js:131 placeDisplay.js:154
#: settings/Menu/ListPinnedPage.js:400
msgid "Computer"
msgstr "电脑"

#: menulayouts/plasma.js:130
msgid "Leave"
msgstr "离开"

#: menulayouts/plasma.js:269 menulayouts/windows.js:271
#: settings/MenuPage.js:123
msgid "Application Shortcuts"
msgstr "应用程序快捷方式"

#: menulayouts/plasma.js:274 menulayouts/windows.js:275
msgid "Places"
msgstr "地点"

#: menulayouts/plasma.js:289 menulayouts/windows.js:207
#: settings/Menu/LayoutTweaksPage.js:748 settings/Menu/LayoutTweaksPage.js:867
msgid "Bookmarks"
msgstr "书签"

#: menulayouts/plasma.js:291 menulayouts/windows.js:209
msgid "Devices"
msgstr "设备"

#: menulayouts/plasma.js:293 menulayouts/windows.js:211
#: settings/Menu/ListPinnedPage.js:401
msgid "Network"
msgstr "网络"

#: menulayouts/pop.js:250 menulayouts/pop.js:252
msgid "Library Home"
msgstr "程序库主页"

#: menulayouts/pop.js:308
msgid "New Folder"
msgstr "新建文件夹"

#: menulayouts/pop.js:342
msgid "Unnamed Folder"
msgstr "未命名文件夹"

#: menulayouts/pop.js:686
msgid "Rename Folder"
msgstr "重命名文件夹"

#: menulayouts/pop.js:687
msgid "Delete Folder"
msgstr "删除文件夹"

#: menulayouts/pop.js:697
#, javascript-format
msgid "Permanently delete %s folder?"
msgstr "永久删除 %s 文件夹吗？"

#: menulayouts/pop.js:702
msgid "No"
msgstr "否"

#: menulayouts/pop.js:710
msgid "Yes"
msgstr "是"

#: menulayouts/pop.js:725
#, javascript-format
msgid "Rename %s folder"
msgstr "重命名 %s 文件夹"

#: menulayouts/pop.js:759
msgid "Cancel"
msgstr "取消"

#: menulayouts/pop.js:767 settings/GeneralPage.js:462
#: settings/Menu/ListPinnedPage.js:637 settings/Menu/VisualSettings.js:365
msgid "Apply"
msgstr "应用"

#: menulayouts/raven.js:61 menulayouts/raven.js:242 menulayouts/unity.js:39
#: menulayouts/unity.js:282 settings/Menu/LayoutTweaksPage.js:586
#: settings/Menu/LayoutTweaksPage.js:621 settings/Menu/LayoutTweaksPage.js:700
#: settings/Menu/LayoutTweaksPage.js:797 settings/Menu/LayoutTweaksPage.js:822
msgid "All Programs"
msgstr "所有程序"

#: menulayouts/raven.js:286 menulayouts/unity.js:356
#: settings/Menu/VisualSettings.js:224
msgid "Shortcuts"
msgstr "快捷方式"

#: menulayouts/runner.js:217
msgid "Configure Runner"
msgstr "配置运行器"

#: menulayouts/unity.js:420
msgid "Categories"
msgstr "类别"

#: menulayouts/windows.js:432
msgid "Extras"
msgstr "附加功能"

#: placeDisplay.js:54
#, javascript-format
msgid "Failed to launch “%s”"
msgstr "启动 “%s” 失败"

#: placeDisplay.js:69
#, javascript-format
msgid "Failed to mount volume for “%s”"
msgstr "无法为 “%s” 安装卷"

#: placeDisplay.js:117 placeDisplay.js:340
#: settings/Menu/LayoutTweaksPage.js:585 settings/Menu/LayoutTweaksPage.js:620
#: settings/Menu/ListPinnedPage.js:392 utils.js:246
msgid "Home"
msgstr "首页"

#: placeDisplay.js:221
#, javascript-format
msgid "Ejecting drive “%s” failed:"
msgstr "弹出驱动器 “%s” 失败："

#: prefsWidgets.js:246
msgid "Modify"
msgstr "修改"

#: prefsWidgets.js:261
msgid "Move Up"
msgstr "上移"

#: prefsWidgets.js:268
msgid "Move Down"
msgstr "下移"

#: prefsWidgets.js:275
msgid "Remove"
msgstr "删除"

#: search.js:726
msgid "Searching..."
msgstr "正在寻找..."

#: search.js:728
msgid "No results."
msgstr "没有结果。"

#: search.js:810
#, javascript-format
msgid "+ %d more"
msgstr "+ %d 更多"

#: searchProviders/openWindows.js:31
msgid "List of open windows across all workspaces"
msgstr "所有工作区中打开的窗口列表"

#: searchProviders/openWindows.js:32
msgid "Open Windows"
msgstr "打开窗口"

#: searchProviders/openWindows.js:46
#, javascript-format
msgid "'%s' on Workspace %d"
msgstr "工作区 %2$d 上的 '%1$s'"

#: searchProviders/recentFiles.js:90
#, javascript-format
msgid "Failed to open “%s”"
msgstr "无法打开 “%s”"

#: settings/AboutPage.js:18 settings/Menu/ListPinnedPage.js:363
msgid "About"
msgstr "关于"

#: settings/AboutPage.js:24
msgid "Application Menu Extension for GNOME"
msgstr "GNOME 的应用程序菜单扩展"

#: settings/AboutPage.js:66
msgid "ArcMenu Version"
msgstr "ArcMenu 版本"

#: settings/AboutPage.js:76
msgid "Git Commit"
msgstr "Git 提交"

#: settings/AboutPage.js:86
msgid "GNOME Version"
msgstr "GNOME 版本"

#: settings/AboutPage.js:95
msgid "OS Name"
msgstr "操作系统名称"

#: settings/AboutPage.js:108
msgid "Windowing System"
msgstr "窗口系统"

#: settings/AboutPage.js:116
msgid "ArcMenu GitLab"
msgstr ""

#: settings/AboutPage.js:119
msgid "Donate via PayPal"
msgstr "通过 PayPal 捐赠"

#: settings/AboutPage.js:131 settings/Menu/ThemingDialog.js:71
#: settings/Menu/ThemingDialog.js:76
msgid "Load"
msgstr "加载"

#: settings/AboutPage.js:136
msgid "Load Settings"
msgstr "加载设置"

#: settings/AboutPage.js:162 settings/Menu/ThemingDialog.js:129
#: settings/Menu/ThemingDialog.js:142
msgid "Save"
msgstr "保存"

#: settings/AboutPage.js:167
msgid "Save Settings"
msgstr "保存设置"

#: settings/AboutPage.js:188
msgid "Credits"
msgstr "制作人员"

#: settings/GeneralPage.js:13
msgid "General"
msgstr "常规"

#: settings/GeneralPage.js:20
msgid "Panel Display Options"
msgstr "面板显示选项"

#: settings/GeneralPage.js:33
msgid "Show Activities Button"
msgstr "显示活动按钮"

#: settings/GeneralPage.js:34
msgid "Dash to Panel may conflict with this setting"
msgstr "Dash to Panel 可能会与此设置冲突"

#: settings/GeneralPage.js:42 settings/GeneralPage.js:68
#: settings/Menu/LayoutTweaksPage.js:648
msgid "Left"
msgstr "左"

#: settings/GeneralPage.js:43 settings/GeneralPage.js:69
msgid "Center"
msgstr "中心"

#: settings/GeneralPage.js:44 settings/GeneralPage.js:70
#: settings/Menu/LayoutTweaksPage.js:649
msgid "Right"
msgstr "右"

#: settings/GeneralPage.js:46 settings/MenuButtonPage.js:124
msgid "Position in Panel"
msgstr "在面板中的位置"

#: settings/GeneralPage.js:76
msgid "Menu Alignment"
msgstr "菜单对齐"

#: settings/GeneralPage.js:95
msgid "Display ArcMenu on all Panels"
msgstr "在所有面板上显示 ArcMenu"

#: settings/GeneralPage.js:96
msgid "Dash to Panel or App Icons Taskbar extension required"
msgstr "需要 Dash to Panel 或应用程序图标任务栏扩展"

#: settings/GeneralPage.js:112
msgid "Always Prefer Top Panel"
msgstr "始终优先选择顶部面板"

#: settings/GeneralPage.js:113
msgid "Useful with Dash to Panel setting 'Keep original gnome-shell top panel'"
msgstr "对于 Dash to Panel 设置 '保留原始 gnome-shell 顶部面板' 很有用"

#: settings/GeneralPage.js:127
msgid "General Settings"
msgstr "常规设置"

#: settings/GeneralPage.js:131
msgid "ArcMenu Hotkey"
msgstr "ArcMenu 快捷键"

#: settings/GeneralPage.js:132
msgid "Standalone Runner Menu"
msgstr "独立运行菜单"

#: settings/GeneralPage.js:145
msgid "Hide Overview on Startup"
msgstr "隐藏启动时的概述"

#: settings/GeneralPage.js:174 settings/GeneralPage.js:293
msgid "Hotkey Conflict"
msgstr "快捷键冲突"

#: settings/GeneralPage.js:175
msgid "ArcMenu and Standalone Runner are assigned the same hotkey"
msgstr "ArcMenu 和 独立运行分配了相同的热键"

#: settings/GeneralPage.js:221
msgid "Open on Primary Monitor"
msgstr "在主显示器上打开"

#: settings/GeneralPage.js:227
msgid "Left Super Key"
msgstr "左 Super 键"

#: settings/GeneralPage.js:228
msgid "Custom Hotkey"
msgstr "自定义快捷键"

#: settings/GeneralPage.js:230
msgid "Hotkey"
msgstr "快捷键"

#: settings/GeneralPage.js:243
msgid "Modify Hotkey"
msgstr "修改快捷键"

#: settings/GeneralPage.js:248
msgid "Current Hotkey"
msgstr "当前快捷键"

#: settings/GeneralPage.js:322
msgid "Set Custom Hotkey"
msgstr "设置自定义快捷键"

#: settings/GeneralPage.js:347
msgid "Choose Modifiers"
msgstr "选择修饰符"

#: settings/GeneralPage.js:357
msgid "Ctrl"
msgstr ""

#: settings/GeneralPage.js:361
msgid "Super"
msgstr ""

#: settings/GeneralPage.js:365
msgid "Shift"
msgstr ""

#: settings/GeneralPage.js:369
msgid "Alt"
msgstr ""

#: settings/GeneralPage.js:437
msgid "Press any key"
msgstr "按任意键"

#: settings/GeneralPage.js:452
msgid "New Hotkey"
msgstr "新快捷键"

#: settings/Menu/FineTunePage.js:32
msgid "Show Category Sub Menus"
msgstr "显示分类子菜单"

#: settings/Menu/FineTunePage.js:46
msgid "Show Application Descriptions"
msgstr "显示应用说明"

#: settings/Menu/FineTunePage.js:60
msgid "Disable ScrollView Fade Effects"
msgstr "禁用滚动视图淡入淡出效果"

#: settings/Menu/FineTunePage.js:74
msgid "Disable Tooltips"
msgstr "禁用工具提示"

#: settings/Menu/FineTunePage.js:88
msgid "Alphabetize 'All Programs' Category"
msgstr "按字母顺序排列 '所有程序' 类别"

#: settings/Menu/FineTunePage.js:102
msgid "Show Hidden Recent Files"
msgstr "显示隐藏的最近文件"

#: settings/Menu/FineTunePage.js:121
msgid "Enable/Disable multi-lined labels on large application icon layouts."
msgstr "启用/禁用大型应用程序图标布局上的多行标签。"

#: settings/Menu/FineTunePage.js:121 settings/Menu/FineTunePage.js:134
msgid "Multi-Lined Labels"
msgstr "多行标签"

#: settings/Menu/FineTunePage.js:145
msgid "Full Color"
msgstr "全彩色"

#: settings/Menu/FineTunePage.js:146
msgid "Symbolic"
msgstr ""

#: settings/Menu/FineTunePage.js:148
msgid "Category Icon Type"
msgstr "类别图标类型"

#: settings/Menu/FineTunePage.js:149 settings/Menu/FineTunePage.js:160
msgid "Some icon themes may not include selected icon type"
msgstr "某些图标主题可能不包含选定的图标类型"

#: settings/Menu/FineTunePage.js:159
msgid "Shortcuts Icon Type"
msgstr "快捷方式图标类型"

#: settings/Menu/FineTunePage.js:183
msgid "Disable New Apps Tracker"
msgstr "禁用新应用程序跟踪器"

#: settings/Menu/FineTunePage.js:192
msgid "Clear All"
msgstr "清除所有"

#: settings/Menu/FineTunePage.js:201
msgid "Clear Apps Marked 'New'"
msgstr "清除标记为 '新' 的应用"

#: settings/Menu/LayoutTweaksPage.js:24 settings/MenuPage.js:49
#: settings/MenuPage.js:91
#, javascript-format
msgid "%s Layout Tweaks"
msgstr "%s 布局调整"

#: settings/Menu/LayoutTweaksPage.js:116
msgid "Vertical Separator"
msgstr "垂直分隔符"

#: settings/Menu/LayoutTweaksPage.js:125
msgid "Mouse Click"
msgstr "鼠标点击"

#: settings/Menu/LayoutTweaksPage.js:126
msgid "Mouse Hover"
msgstr "鼠标悬停"

#: settings/Menu/LayoutTweaksPage.js:129
msgid "Category Activation"
msgstr "类别激活"

#: settings/Menu/LayoutTweaksPage.js:152
msgid "Round"
msgstr "圆形"

#: settings/Menu/LayoutTweaksPage.js:153 settings/Menu/VisualSettings.js:178
#: settings/Menu/VisualSettings.js:179 settings/Menu/VisualSettings.js:180
msgid "Square"
msgstr "方形"

#: settings/Menu/LayoutTweaksPage.js:155
msgid "Avatar Icon Shape"
msgstr "Avatar 图标形状"

#: settings/Menu/LayoutTweaksPage.js:171 settings/Menu/LayoutTweaksPage.js:888
msgid "Bottom"
msgstr "底部"

#: settings/Menu/LayoutTweaksPage.js:172 settings/Menu/LayoutTweaksPage.js:466
#: settings/Menu/LayoutTweaksPage.js:889
msgid "Top"
msgstr "顶部"

#: settings/Menu/LayoutTweaksPage.js:175
msgid "Searchbar Location"
msgstr "搜索栏位置"

#: settings/Menu/LayoutTweaksPage.js:196
msgid "Flip Layout Horizontally"
msgstr "水平翻转布局"

#: settings/Menu/LayoutTweaksPage.js:212
msgid "Disable User Avatar"
msgstr "禁用用户头像"

#: settings/Menu/LayoutTweaksPage.js:252 settings/Menu/LayoutTweaksPage.js:588
#: settings/Menu/LayoutTweaksPage.js:623 settings/Menu/LayoutTweaksPage.js:704
#: settings/Menu/LayoutTweaksPage.js:799 settings/Menu/LayoutTweaksPage.js:824
msgid "Default View"
msgstr "默认视图"

#: settings/Menu/LayoutTweaksPage.js:279 settings/Menu/LayoutTweaksPage.js:348
msgid "Disable Frequent Apps"
msgstr "禁用常用应用程序"

#: settings/Menu/LayoutTweaksPage.js:287 settings/Menu/LayoutTweaksPage.js:290
#: settings/Menu/LayoutTweaksPage.js:306 settings/Menu/LayoutTweaksPage.js:309
#: settings/Menu/LayoutTweaksPage.js:371 settings/Menu/LayoutTweaksPage.js:374
#: settings/Menu/LayoutTweaksPage.js:602 settings/Menu/LayoutTweaksPage.js:605
#: settings/Menu/LayoutTweaksPage.js:673 settings/Menu/LayoutTweaksPage.js:676
#: settings/Menu/LayoutTweaksPage.js:762 settings/Menu/LayoutTweaksPage.js:765
msgid "Button Shortcuts"
msgstr "按钮快捷键"

#: settings/Menu/LayoutTweaksPage.js:329
msgid "Show Apps Grid"
msgstr "显示应用程序网格"

#: settings/Menu/LayoutTweaksPage.js:362
msgid "Disable Pinned Apps"
msgstr "禁用固定应用程序"

#: settings/Menu/LayoutTweaksPage.js:396
msgid "Activate on Hover"
msgstr "悬停时激活"

#: settings/Menu/LayoutTweaksPage.js:414 settings/Menu/LayoutTweaksPage.js:417
#: settings/Menu/LayoutTweaksPage.js:722 settings/Menu/LayoutTweaksPage.js:842
msgid "Extra Shortcuts"
msgstr "额外的快捷方式"

#: settings/Menu/LayoutTweaksPage.js:454
msgid "Enable Activities Overview Shortcut"
msgstr "启用活动概览快捷方式"

#: settings/Menu/LayoutTweaksPage.js:467
msgid "Centered"
msgstr "居中"

#: settings/Menu/LayoutTweaksPage.js:469
msgid "Position"
msgstr "位置"

#: settings/Menu/LayoutTweaksPage.js:480 settings/Menu/LayoutTweaksPage.js:634
msgid "List"
msgstr "列表"

#: settings/Menu/LayoutTweaksPage.js:481 settings/Menu/LayoutTweaksPage.js:635
msgid "Grid"
msgstr "网格"

#: settings/Menu/LayoutTweaksPage.js:483 settings/Menu/LayoutTweaksPage.js:637
msgid "Search Results Display Style"
msgstr "搜索结果显示方式"

#: settings/Menu/LayoutTweaksPage.js:510 settings/Menu/VisualSettings.js:308
msgid "Width"
msgstr "宽度"

#: settings/Menu/LayoutTweaksPage.js:533 settings/Menu/VisualSettings.js:36
#: settings/Menu/VisualSettings.js:327
msgid "Height"
msgstr "高度"

#: settings/Menu/LayoutTweaksPage.js:556 settings/Menu/ThemePage.js:168
msgid "Font Size"
msgstr "字体大小"

#: settings/Menu/LayoutTweaksPage.js:557 settings/MenuButtonPage.js:101
#, javascript-format
msgid "%d Default Theme Value"
msgstr "%d 默认主题值"

#: settings/Menu/LayoutTweaksPage.js:571
msgid "Show Frequent Apps"
msgstr "显示常用应用程序"

#: settings/Menu/LayoutTweaksPage.js:651
msgid "Position on Monitor"
msgstr "显示器上的位置"

#: settings/Menu/LayoutTweaksPage.js:734 settings/Menu/LayoutTweaksPage.js:853
msgid "External Devices"
msgstr "外部设备"

#: settings/Menu/LayoutTweaksPage.js:786
msgid "Nothing Yet!"
msgstr "还没有！"

#: settings/Menu/LayoutTweaksPage.js:796 settings/Menu/LayoutTweaksPage.js:820
msgid "Categories List"
msgstr "类别列表"

#: settings/Menu/LayoutTweaksPage.js:875
msgid "Category Quick Links"
msgstr "类别快速链接"

#: settings/Menu/LayoutTweaksPage.js:876
msgid ""
"Display quick links of extra categories on the home page\n"
"Must also be enabled in 'Menu -> Extra Categories' section"
msgstr ""
"在主页上显示额外类别的快速链接\n"
"还必须在 '菜单 -> 额外类别' 部分中启用"

#: settings/Menu/LayoutTweaksPage.js:891
msgid "Quick Links Location"
msgstr "快速链接位置"

#: settings/Menu/LayoutTweaksPage.js:923
msgid "Enable Weather Widget"
msgstr "启用天气小工具"

#: settings/Menu/LayoutTweaksPage.js:937
msgid "Enable Clock Widget"
msgstr "启用时钟小部件"

#: settings/Menu/LayoutsPage.js:24
msgid "Current Menu Layout"
msgstr "当前菜单布局"

#: settings/Menu/LayoutsPage.js:38
msgid "Choose a new menu layout?"
msgstr "选择新的菜单布局？"

#: settings/Menu/LayoutsPage.js:44
#, javascript-format
msgid "%s Menu Layouts"
msgstr "%s 菜单布局"

#: settings/Menu/ListOtherPage.js:34
msgid "Actions will be hidden from ArcMenu if not available on your system."
msgstr "如果您的系统上不可用，则操作将从 ArcMenu 中隐藏。"

#: settings/Menu/ListOtherPage.js:38
msgid "Power Off / Log Out Buttons"
msgstr "关机/注销按钮"

#: settings/Menu/ListOtherPage.js:41 settings/Menu/VisualSettings.js:111
#: settings/Menu/VisualSettings.js:177 settings/Menu/VisualSettings.js:271
msgid "Off"
msgstr "关闭"

#: settings/Menu/ListOtherPage.js:42
msgid "Power Buttons"
msgstr "电源按钮"

#: settings/Menu/ListOtherPage.js:43
msgid "Power Menu"
msgstr "电源菜单"

#: settings/Menu/ListOtherPage.js:45
msgid "Override Display Style"
msgstr "覆盖显示风格"

#: settings/Menu/ListPinnedPage.js:35 settings/Menu/ListPinnedPage.js:38
#: settings/Menu/ListPinnedPage.js:44
msgid "Add More Apps"
msgstr "添加更多应用程序"

#: settings/Menu/ListPinnedPage.js:41
msgid "Add Default User Directories"
msgstr "添加默认用户目录"

#: settings/Menu/ListPinnedPage.js:47
msgid "Add More Shortcuts"
msgstr "添加更多快捷方式"

#: settings/Menu/ListPinnedPage.js:123
msgid "Add Custom Shortcut"
msgstr "添加自定义快捷方式"

#: settings/Menu/ListPinnedPage.js:242
msgid "Invalid Shortcut"
msgstr "无效的快捷方式"

#: settings/Menu/ListPinnedPage.js:319
msgid "Add to your Pinned Apps"
msgstr "添加到您的固定应用程序"

#: settings/Menu/ListPinnedPage.js:321
msgid "Add to your Extra Shortcuts"
msgstr "添加到您的额外快捷方式"

#: settings/Menu/ListPinnedPage.js:323
msgid "Select Application Shortcuts"
msgstr "选择应用程序快捷方式"

#: settings/Menu/ListPinnedPage.js:325
msgid "Select Directory Shortcuts"
msgstr "选择目录快捷方式"

#: settings/Menu/ListPinnedPage.js:327
msgid "Add to the Context Menu"
msgstr "添加到上下文菜单"

#: settings/Menu/ListPinnedPage.js:345
msgid "Run Command..."
msgstr "运行命令..."

#: settings/Menu/ListPinnedPage.js:348
msgid "Show All Apps"
msgstr "显示所有应用程序"

#: settings/Menu/ListPinnedPage.js:355
msgid "Menu Settings"
msgstr "菜单设置"

#: settings/Menu/ListPinnedPage.js:357
msgid "Menu Theming"
msgstr "菜单主题"

#: settings/Menu/ListPinnedPage.js:359
msgid "Change Menu Layout"
msgstr "更改菜单布局"

#: settings/Menu/ListPinnedPage.js:361
msgid "Menu Button Settings"
msgstr "菜单按钮设置"

#: settings/Menu/ListPinnedPage.js:364
msgid "Panel Extension Settings"
msgstr "面板扩展设置"

#: settings/Menu/ListPinnedPage.js:368 settings/MenuPage.js:144
msgid "Power Options"
msgstr "电源选项"

#: settings/Menu/ListPinnedPage.js:370 settings/Menu/ListPinnedPage.js:375
msgid "Separator"
msgstr "分隔器"

#: settings/Menu/ListPinnedPage.js:393
msgid "Documents"
msgstr "文件"

#: settings/Menu/ListPinnedPage.js:395
msgid "Downloads"
msgstr "下载"

#: settings/Menu/ListPinnedPage.js:397
msgid "Music"
msgstr "音乐"

#: settings/Menu/ListPinnedPage.js:398
msgid "Pictures"
msgstr "图片"

#: settings/Menu/ListPinnedPage.js:399
msgid "Videos"
msgstr "视频"

#: settings/Menu/ListPinnedPage.js:402
msgid "Recent"
msgstr "最近的"

#: settings/Menu/ListPinnedPage.js:432
msgid "Dash to Panel or App Icons Taskbar"
msgstr "Dash to Panel 或应用程序图标任务栏"

#: settings/Menu/ListPinnedPage.js:514
#, javascript-format
msgid "%s has been added"
msgstr "%s 已添加"

#: settings/Menu/ListPinnedPage.js:525
#, javascript-format
msgid "%s has been removed"
msgstr "%s 已被删除"

#: settings/Menu/ListPinnedPage.js:548
msgid "Add a Custom Shortcut"
msgstr "添加自定义快捷方式"

#: settings/Menu/ListPinnedPage.js:553
msgid "Edit Pinned App"
msgstr "编辑固定的应用程序"

#: settings/Menu/ListPinnedPage.js:555
msgid "Edit Shortcut"
msgstr "编辑快捷方式"

#: settings/Menu/ListPinnedPage.js:566
msgid "Title"
msgstr "标题"

#: settings/Menu/ListPinnedPage.js:578 settings/MenuButtonPage.js:57
msgid "Icon"
msgstr "图标"

#: settings/Menu/ListPinnedPage.js:590 settings/MenuButtonPage.js:156
#: settings/MenuButtonPage.js:460
msgid "Browse..."
msgstr "浏览..."

#: settings/Menu/ListPinnedPage.js:596 settings/MenuButtonPage.js:465
msgid "Select an Icon"
msgstr "选择一个图标"

#: settings/Menu/ListPinnedPage.js:625
msgid "Command"
msgstr "命令"

#: settings/Menu/ListPinnedPage.js:625
msgid "Directory"
msgstr "目录"

#: settings/Menu/ListPinnedPage.js:637
msgid "Add"
msgstr "添加"

#: settings/Menu/SearchOptionsPage.js:22
msgid "Extra Search Providers"
msgstr "额外的搜索提供商"

#: settings/Menu/SearchOptionsPage.js:33
msgid "Search for open windows across all workspaces"
msgstr "搜索所有工作区中打开的窗口"

#: settings/Menu/SearchOptionsPage.js:47
msgid "Search for recent files"
msgstr "搜索最近的文件"

#: settings/Menu/SearchOptionsPage.js:55 settings/MenuPage.js:134
msgid "Search Options"
msgstr "搜索选项"

#: settings/Menu/SearchOptionsPage.js:66
msgid "Show Search Result Descriptions"
msgstr "显示搜索结果说明"

#: settings/Menu/SearchOptionsPage.js:81
msgid "Highlight search result terms"
msgstr "突出显示搜索结果词"

#: settings/Menu/SearchOptionsPage.js:100
msgid "Max Search Results"
msgstr "最大搜索结果"

#: settings/Menu/SearchOptionsPage.js:144
msgid "Search Box Border Radius"
msgstr "搜索框边框圆角"

#: settings/Menu/SubPage.js:53 settings/MenuButtonPage.js:27
msgid "Reset settings"
msgstr "重新设置"

#: settings/Menu/SubPage.js:58 settings/MenuButtonPage.js:35
#, javascript-format
msgid "Reset all %s settings?"
msgstr "重置所有 %s 设置吗？"

#: settings/Menu/SubPage.js:59 settings/MenuButtonPage.js:36
#, javascript-format
msgid "All %s settings will be reset to the default value."
msgstr "所有 %s 设置将重置为默认值。"

#: settings/Menu/ThemePage.js:38
msgid "Override Theme"
msgstr "覆盖主题"

#: settings/Menu/ThemePage.js:39 settings/MenuButtonPage.js:202
msgid "Results may vary with third party themes"
msgstr "结果可能因第三方主题而有差异"

#: settings/Menu/ThemePage.js:46 settings/Menu/ThemingDialog.js:68
#: settings/Menu/ThemingDialog.js:76 settings/Menu/ThemingDialog.js:142
msgid "Menu Themes"
msgstr "菜单主题"

#: settings/Menu/ThemePage.js:90
msgid "Current Theme"
msgstr "当前主题"

#: settings/Menu/ThemePage.js:118
msgid "Save as Theme"
msgstr "另存为主题"

#: settings/Menu/ThemePage.js:148
msgid "Menu Styling"
msgstr "菜单样式"

#: settings/Menu/ThemePage.js:153 settings/Menu/ThemePage.js:179
#: settings/Menu/ThemePage.js:185 settings/MenuButtonPage.js:209
#: settings/MenuButtonPage.js:212 settings/MenuButtonPage.js:218
msgid "Background Color"
msgstr "背景颜色"

#: settings/Menu/ThemePage.js:156 settings/Menu/ThemePage.js:182
#: settings/Menu/ThemePage.js:188 settings/MenuButtonPage.js:206
#: settings/MenuButtonPage.js:215 settings/MenuButtonPage.js:221
msgid "Foreground Color"
msgstr "前景色"

#: settings/Menu/ThemePage.js:159 settings/MenuButtonPage.js:232
msgid "Border Color"
msgstr "边框颜色"

#: settings/Menu/ThemePage.js:162 settings/MenuButtonPage.js:228
msgid "Border Width"
msgstr "边框宽度"

#: settings/Menu/ThemePage.js:165 settings/MenuButtonPage.js:224
msgid "Border Radius"
msgstr "边框圆角"

#: settings/Menu/ThemePage.js:171
msgid "Separator Color"
msgstr "分隔符颜色"

#: settings/Menu/ThemePage.js:175
msgid "Menu Items Styling"
msgstr "菜单项样式"

#: settings/Menu/ThemePage.js:179 settings/Menu/ThemePage.js:182
#: settings/MenuButtonPage.js:212 settings/MenuButtonPage.js:215
msgid "Hover"
msgstr "悬停"

#: settings/Menu/ThemePage.js:185 settings/Menu/ThemePage.js:188
#: settings/MenuButtonPage.js:218 settings/MenuButtonPage.js:221
msgid "Active"
msgstr "激活"

#: settings/Menu/ThemingDialog.js:16
msgid "Save Theme As..."
msgstr "将主题另存为..."

#: settings/Menu/ThemingDialog.js:28
msgid "Theme Name"
msgstr "主题名称"

#: settings/Menu/ThemingDialog.js:45
msgid "Save Theme"
msgstr "保存主题"

#: settings/Menu/ThemingDialog.js:61
msgid "Manage Themes"
msgstr "管理主题"

#: settings/Menu/ThemingDialog.js:316
msgid "Save Themes"
msgstr "保存主题"

#: settings/Menu/ThemingDialog.js:318
msgid "Load Themes"
msgstr "加载主题"

#: settings/Menu/ThemingDialog.js:398
#, javascript-format
msgid "%s has been selected"
msgstr "%s 已被选择"

#: settings/Menu/ThemingDialog.js:410
#, javascript-format
msgid "%s has been unselected"
msgstr "%s 已被取消选择"

#: settings/Menu/VisualSettings.js:18
msgid "Menu Size"
msgstr "菜单大小"

#: settings/Menu/VisualSettings.js:56
msgid "Left-Panel Width"
msgstr "左面板宽度"

#: settings/Menu/VisualSettings.js:57 settings/Menu/VisualSettings.js:78
msgid "Traditional Layouts"
msgstr "传统布局"

#: settings/Menu/VisualSettings.js:77
msgid "Right-Panel Width"
msgstr "右面板宽度"

#: settings/Menu/VisualSettings.js:98
msgid "Width Offset"
msgstr "宽度偏移"

#: settings/Menu/VisualSettings.js:99 settings/Menu/VisualSettings.js:186
msgid "Non-Traditional Layouts"
msgstr "非传统布局"

#: settings/Menu/VisualSettings.js:106
msgid "Menu Location"
msgstr "菜单位置"

#: settings/Menu/VisualSettings.js:112
msgid "Top Centered"
msgstr "顶部居中"

#: settings/Menu/VisualSettings.js:113
msgid "Bottom Centered"
msgstr "底部居中"

#: settings/Menu/VisualSettings.js:115
msgid "Override Menu Location"
msgstr "覆盖菜单位置"

#: settings/Menu/VisualSettings.js:156
msgid "Override Menu Rise"
msgstr "覆盖菜单上升"

#: settings/Menu/VisualSettings.js:157
msgid "Menu Distance from Panel and Screen Edge"
msgstr "菜单与面板和屏幕边缘的距离"

#: settings/Menu/VisualSettings.js:171
msgid "Override Icon Sizes"
msgstr "覆盖图标大小"

#: settings/Menu/VisualSettings.js:172
msgid "Override the icon size of various menu items"
msgstr "覆盖各种菜单项的图标大小"

#: settings/Menu/VisualSettings.js:178 settings/Menu/VisualSettings.js:181
#: settings/Menu/VisualSettings.js:273
msgid "Small"
msgstr "小"

#: settings/Menu/VisualSettings.js:179 settings/Menu/VisualSettings.js:182
#: settings/Menu/VisualSettings.js:274
msgid "Medium"
msgstr "中"

#: settings/Menu/VisualSettings.js:180 settings/Menu/VisualSettings.js:183
#: settings/Menu/VisualSettings.js:275
msgid "Large"
msgstr "大"

#: settings/Menu/VisualSettings.js:181 settings/Menu/VisualSettings.js:182
#: settings/Menu/VisualSettings.js:183
msgid "Wide"
msgstr "宽"

#: settings/Menu/VisualSettings.js:184
msgid "Custom"
msgstr "自定义"

#: settings/Menu/VisualSettings.js:186
msgid "Grid Menu Items"
msgstr "网格菜单项"

#: settings/Menu/VisualSettings.js:187
msgid "Apps, Pinned Apps, Shortcuts, Grid Search Results"
msgstr "应用程序、固定应用程序、快捷方式、网格搜索结果"

#: settings/Menu/VisualSettings.js:217
msgid "Applications"
msgstr "应用"

#: settings/Menu/VisualSettings.js:218
msgid "Apps, Pinned Apps, Items within Category, List Search Results"
msgstr "应用程序、固定应用程序、类别内的项目、列表搜索结果"

#: settings/Menu/VisualSettings.js:225
msgid "Directory / Application / Other Shortcuts, Power Menu"
msgstr "目录/应用程序/其他快捷方式、电源菜单"

#: settings/Menu/VisualSettings.js:231
msgid "Application Categories"
msgstr "应用类别"

#: settings/Menu/VisualSettings.js:237
msgid "Button Widgets"
msgstr "按钮小部件"

#: settings/Menu/VisualSettings.js:238
msgid "Power Buttons, Unity Bottom Bar, Mint Side Bar, etc"
msgstr "电源按钮、Unity 底栏、Mint 侧栏等"

#: settings/Menu/VisualSettings.js:244
msgid "Miscellaneous"
msgstr "其他"

#: settings/Menu/VisualSettings.js:245
msgid "Avatar, Search, Navigation Icons"
msgstr "头像、搜索、导航图标"

#: settings/Menu/VisualSettings.js:272
msgid "Extra Small"
msgstr "特小"

#: settings/Menu/VisualSettings.js:276
msgid "Extra Large"
msgstr "特大"

#: settings/Menu/VisualSettings.js:279 settings/MenuButtonPage.js:61
msgid "Hidden"
msgstr "隐藏"

#: settings/Menu/VisualSettings.js:298
msgid "Custom Grid Icon Size"
msgstr "自定义网格图标大小"

#: settings/Menu/VisualSettings.js:346 settings/MenuButtonPage.js:190
msgid "Icon Size"
msgstr "图标大小"

#: settings/MenuButtonPage.js:16
msgid "Menu Button"
msgstr "菜单按钮"

#: settings/MenuButtonPage.js:52
msgid "Menu Button Appearance"
msgstr "菜单按钮外观"

#: settings/MenuButtonPage.js:58 settings/MenuButtonPage.js:141
msgid "Text"
msgstr "文本"

#: settings/MenuButtonPage.js:59
msgid "Icon and Text"
msgstr "图标和文本"

#: settings/MenuButtonPage.js:60
msgid "Text and Icon"
msgstr "文本和图标"

#: settings/MenuButtonPage.js:63
msgid "Display Style"
msgstr "显示风格"

#: settings/MenuButtonPage.js:100
msgid "Padding"
msgstr "填充"

#: settings/MenuButtonPage.js:153
msgid "Menu Button Icon"
msgstr "菜单按钮图标"

#: settings/MenuButtonPage.js:167
msgid "Choose a new icon"
msgstr "选择一个新图标"

#: settings/MenuButtonPage.js:201
msgid "Menu Button Styling"
msgstr "菜单按钮样式"

#: settings/MenuButtonPage.js:229
msgid "Background colors required if set to 0"
msgstr "如果设置为 0，则需要背景颜色"

#: settings/MenuButtonPage.js:377 settings/MenuButtonPage.js:381
msgid "ArcMenu Icons"
msgstr "ArcMenu 图标"

#: settings/MenuButtonPage.js:401
msgid "Distro Icons"
msgstr "发行版图标"

#: settings/MenuButtonPage.js:426 settings/MenuButtonPage.js:491
msgid "Custom Icon"
msgstr "自定义图标"

#: settings/MenuButtonPage.js:549
msgid "Legal disclaimer for Distro Icons"
msgstr "发行版图标的法律免责声明"

#: settings/MenuPage.js:24
msgid "Menu"
msgstr "菜单"

#: settings/MenuPage.js:32
msgid "How should the menu look?"
msgstr "菜单应该是什么样子？"

#: settings/MenuPage.js:39
msgid "Menu Layout"
msgstr "菜单布局"

#: settings/MenuPage.js:40
msgid "Choose a layout style for the menu"
msgstr "选择菜单的布局样式"

#: settings/MenuPage.js:55
msgid "Menu Theme"
msgstr "菜单主题"

#: settings/MenuPage.js:56
msgid "Modify menu colors, font size, and border"
msgstr "修改菜单颜色、字体大小和边框"

#: settings/MenuPage.js:65
msgid "Menu Visual Appearance"
msgstr "菜单视觉外观"

#: settings/MenuPage.js:66
msgid "Change menu height, width, location, and icon sizes"
msgstr "更改菜单高度、宽度、位置和图标大小"

#: settings/MenuPage.js:75
msgid "Fine Tune"
msgstr "微调"

#: settings/MenuPage.js:76
msgid "Adjust less commonly used visual settings"
msgstr "调整不常用的视觉设置"

#: settings/MenuPage.js:85
msgid "What should show on the menu?"
msgstr "菜单上应该显示什么？"

#: settings/MenuPage.js:92
msgid "Settings specific to the current menu layout"
msgstr "当前菜单布局特定的设置"

#: settings/MenuPage.js:112
msgid "Directory Shortcuts"
msgstr "目录快捷方式"

#: settings/MenuPage.js:145
msgid "Choose which power options to show and the display style"
msgstr "选择要显示的电源选项以及显示样式"

#: settings/MenuPage.js:156
msgid "Extra Categories"
msgstr "额外类别"

#: settings/MenuPage.js:158
msgid "Add or remove additional custom categories"
msgstr "添加或删除其他自定义类别"

#: settings/MenuPage.js:168
msgid "What should show on the context menu?"
msgstr "上下文菜单上应该显示什么？"

#: settings/MenuPage.js:173
msgid "Modify ArcMenu Context Menu"
msgstr "修改 ArcMenu 上下文菜单"

